<?php

namespace App\Helpers;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class BuilderHelper
{
    /**
     * @OA\Parameter(
     *   description="Enable pagination",
     *   in="query",
     *   name="paginate",
     *   required=false,
     *   @OA\Schema(
     *     type="boolean",
     *     default=true,
     *   ),
     * ),
     * @OA\Parameter(
     *   description="Pagination page",
     *   in="query",
     *   name="page",
     *   required=false,
     *   @OA\Schema(
     *     type="integer",
     *     default=1,
     *   ),
     * ),
     * @OA\Parameter(
     *   description="Pagination per page",
     *   in="query",
     *   name="per_page",
     *   required=false,
     *   @OA\Schema(
     *     type="integer",
     *     default=15,
     *   ),
     * ),
     * Enable pagination
     *
     * @param Builder $builder
     *
     * @return LengthAwarePaginator|Builder[]|Collection
     */
    public static function getOrPaginate(Builder $builder)
    {
        $paginate = request()->get('paginate');

        if ($paginate === 'false' || $paginate === '0') {
            return $builder->get();
        }

        return $builder->paginate((int)request()->get('per_page', $builder->getModel()->getPerPage()));
    }
}
