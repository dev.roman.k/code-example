<?php

namespace App\Http\Requests;

interface HasSorts
{
    public function validateSorts();
}
