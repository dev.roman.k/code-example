<?php

namespace App\Policies;

use App\Classes\Types\PermissionType;
use App\Classes\Types\RoleType;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     *
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can(PermissionType::VIEW_USERS);
    }

    /**
     * Determine whether the user can view customer models.
     *
     * @param User $user
     *
     * @return mixed
     */
    public function viewCustomers(User $user)
    {
        return $user->can(PermissionType::VIEW_CUSTOMERS);
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param User $model
     *
     * @return mixed
     */
    public function view(User $user, User $model)
    {
        if ($user->id === $model->id) {
            return true;
        }

        return $user->can(PermissionType::VIEW_USERS);
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can(PermissionType::EDIT_USERS);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param User $model
     *
     * @return mixed
     */
    public function update(User $user, User $model)
    {
        // only users with "admin" role can assign "admin" role to other users
        if (request()->get('role_name') === RoleType::ADMIN_ROLE) {
            return $user->role_name === RoleType::ADMIN_ROLE;
        }

        return $user->can(PermissionType::EDIT_USERS);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User $user
     * @param User $model
     *
     * @return mixed
     */
    public function delete(User $user, User $model)
    {
        // only users with "admin" role can delete other users
        if ($model->role_name === RoleType::ADMIN_ROLE) {
            return $user->role_name === RoleType::ADMIN_ROLE;
        }

        return $user->can(PermissionType::EDIT_USERS);
    }

    /**
     * @param User $user
     * @param User $model
     *
     * @return bool
     */
    public function changePassword(User $user, User $model): bool
    {
        // only user can change own password
        return $user->id === $model->id;
    }
}
