<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Builder;

trait Filterable
{
    /**
     * @param Builder $query
     * @param array   $filters
     */
    public function scopeFilter(Builder $query, array $filters): void
    {
        // filter: [$key, $operator, $value]
        foreach ($filters as $filter) {
            $this->applyFilter($query, ...$filter);
        }
    }

    /**
     * @param Builder $query
     * @param string  $key
     * @param string  $operator
     * @param null    $value
     */
    private function applyFilter(Builder $query, string $key, string $operator, $value = null): void
    {
        $isRelationKey = strpos($key, '.') !== false;
        if ($isRelationKey) {
            [$relation, $key] = preg_split('/\.(?=[^.]*$)/', $key);
            $query->whereHas($relation, function (Builder $q) use ($key, $operator, $value) {
                $q->where($key, $operator, $value);
            });
        } else {
            $query->where($key, $operator, $value);
        }
    }
}
