<?php

namespace App\Classes\Types;

final class RoleType
{
    public const ADMIN_ROLE = 'admin';
    public const USER_ROLE = 'user';
    public const MASTER_ROLE = 'master';

    public const ROLES = [
        self::ADMIN_ROLE,
        self::USER_ROLE,
        self::MASTER_ROLE,
    ];

    public const ROLES_PERMISSIONS = [
        self::ADMIN_ROLE => PermissionType::PERMISSIONS
    ];
}
