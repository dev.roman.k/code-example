<?php

namespace App\Http\Controllers\Api\Admin;

use App\Classes\Types\RoleType;
use App\Helpers\BuilderHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\IndexCustomerRequest;
use App\Http\Requests\ShowCustomerRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CustomerController extends Controller
{

    /**
     * @OA\Get(
     *   tags={"Admin/Customers"},
     *   path="/api/v1/admin/customers",
     *   summary="Customers list",
     *   parameters={
     *     {"$ref": "#/components/parameters/paginate"},
     *   },
     *   @OA\Response(
     *     response=200,
     *     description="A list of customers",
     *     ref="#/components/responses/UserResourceColllection"
     *   ),
     *   @OA\Response(
     *     response=403,
     *     description="Forbidden",
     *     @OA\JsonContent()
     *   ),
     *   @OA\Response(
     *     response=401,
     *     description="Unauthenticated",
     *     @OA\JsonContent()
     *   ),
     *   security={{"bearer":{}}}
     * )
     * @param IndexCustomerRequest $request
     *
     * @return AnonymousResourceCollection
     * @throws AuthorizationException
     */
    public function index(IndexCustomerRequest $request): AnonymousResourceCollection
    {
        $this->authorize('viewCustomers', User::class);

        return UserResource::collection(
            BuilderHelper::getOrPaginate(
                User::query()
                    ->whereHas('roles', function (Builder $query) {
                        $query->where('name', RoleType::USER_ROLE);
                    })
                    ->with($request->getIncludes())
                    ->filter($request->getFilters())
                    ->sort($request->getSorts())
            )
        );
    }

    /**
     * @OA\Get(
     *   tags={"Admin/Customers"},
     *   path="/api/v1/admin/customers/{customer_id}",
     *   summary="Customer info",
     *   @OA\Parameter(
     *     description="customer id",
     *     in="path",
     *     name="customer_id",
     *     required=true,
     *     @OA\Schema(
     *       type="string",
     *     ),
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Customer information",
     *     ref="#/components/responses/UserResource",
     *   ),
     *   @OA\Response(
     *     response=401,
     *     description="Unauthorized",
     *     @OA\JsonContent(),
     *   ),
     *   security={{"bearer":{}}}
     * )
     * @param ShowCustomerRequest $request
     * @param User                $customer
     *
     * @return UserResource
     * @throws AuthorizationException
     */
    public function show(ShowCustomerRequest $request, User $customer): UserResource
    {
        $this->authorize('viewCustomers', $customer);
        abort_if($customer->role_name != 'user', 404);
        return UserResource::make($customer->load($request->getIncludes()));
    }
}
