<?php

namespace App\Http\Requests\Traits;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

trait HasFilters
{
    /**
     * List of allowed attributes to filter.
     *
     * @return array
     */
    public function allowedFilters(): array
    {
        // restrict all attributes to filter by default
        return [];
    }

    /**
     * Validation rules for filter array.
     *
     * @return array
     */
    public function filterKeyRules(): array
    {
        return [
            'filter'   => 'sometimes|required|array',
            'filter.*' => 'required|string|min:0'
        ];
    }

    /**
     * @throws ValidationException
     */
    public function validateFilters(): void
    {
        // add validation rules to validator
        /** @var Validator $validator */
        $validator = $this->getValidatorInstance();

        $rules = $this->filterKeyRules();
        $validator->addRules($rules);
        if ($validator->fails()) {
            $this->failedValidation($validator);
        }

        $this->validateFilterKeys();
    }

    /**
     * @return array
     */
    public function getFilters(): array
    {
        $validatedFilters = $this->getValidatedFilters();

        $queryFilters = [];
        foreach ($validatedFilters as $parameter => $filter) {
            $filterSets = explode(',', $filter);
            foreach ($filterSets as $filterSet) {
                [$operator, $value] = $this->parseFilterSet($filterSet);
                $queryFilter = [$parameter, $operator, $value];
                $queryFilters[] = $queryFilter;
            }
        }

        return $queryFilters;
    }

    /**
     * @throws ValidationException
     */
    private function validateFilterKeys(): void
    {
        $requestFilters = $this->getRequestFilters();
        $allowedFilters = $this->allowedFilters();

        $re = "/^(\w+(?:\:[\w%]+|\:)?)(,(?1))*$/";
        foreach ($requestFilters as $parameter => $filter) {
            $isAllowed = in_array($parameter, $allowedFilters, true);
            $validValue = preg_match($re, $filter);
            if (!$isAllowed || !$validValue) {
                throw ValidationException::withMessages(['filter' => "invalid filter parameter ${parameter}"]);
            }
        }
    }

    private function getValidatedFilters(): array
    {
        $requestFilters = $this->getRequestFilters();
        $allowedFilters = $this->allowedFilters();

        $validatedFilters = array_filter($requestFilters, static function ($key) use ($allowedFilters) {
            return in_array($key, $allowedFilters, true);
        }, ARRAY_FILTER_USE_KEY);

        return $validatedFilters;
    }

    /**
     * Parse filter set like <operator:value>|<value>|<operator:>
     *
     * @param string $filterSet
     *
     * @return array
     */
    private function parseFilterSet(string $filterSet): array
    {
        $operators = $this->operators();

        $arguments = explode(':', $filterSet);
        // set default operator
        if (count($arguments) === 1) {
            [$operator, $value] = ['eq', ...$arguments];
        } else {
            [$operator, $value] = $arguments;
        }

        $operator = $operators[$operator];
        // check null conditions
        if ($operator === 'isnull') {
            $operator = '=';
            $value = null;
        }
        if ($operator === 'isnotnull') {
            $operator = '<>';
            $value = null;
        }

        return [$operator, $value];
    }

    /**
     * @return array
     */
    private function getRequestFilters(): array
    {
        return $this->get('filter', []);
    }

    /**
     * List of allowed operators.
     *
     * @return array
     */
    private function operators(): array
    {
        return [
            'eq'        => '=',
            'ne'        => '!=',
            'gt'        => '>',
            'ge'        => '>=',
            'lt'        => '<',
            'le'        => '<=',
            'like'      => 'like',
            'isnull'    => 'isnull',
            'isnotnull' => 'isnotnull'
        ];
    }
}
