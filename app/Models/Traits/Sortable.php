<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Builder;

trait Sortable
{
    /**
     * @param Builder $query
     * @param array   $sorts
     */
    public function scopeSort(Builder $query, array $sorts): void
    {
        // sort: [$column, $direction]
        foreach ($sorts as $sort) {
            $this->applySort($query, ...$sort);
        }
    }

    private function applySort(Builder $query, string $column, string $direction): void
    {
        $query->orderBy($column, $direction);
    }
}
