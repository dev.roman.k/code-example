<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * @OA\Schema(
     *   schema="User",
     *   @OA\Property(
     *     property="id",
     *     type="integer"
     *   ),
     *   @OA\Property(
     *     property="first_name",
     *     type="string"
     *   ),
     *   @OA\Property(
     *     property="email",
     *     type="string",
     *     format="email"
     *   ),
     *   @OA\Property(
     *     property="phone",
     *     type="integer"
     *   ),
     *   @OA\Property(
     *     property="last_name",
     *     type="string"
     *   ),
     *   @OA\Property(
     *     property="adv_notification",
     *     type="boolean"
     *   ),
     *   @OA\Property(
     *     property="repair_notification",
     *     type="boolean"
     *   ),
     *   @OA\Property(
     *     property="role_name",
     *     type="string"
     *   ),
     *   @OA\Property(
     *     property="address",
     *     type="string"
     *   ),
     *   @OA\Property(
     *     property="gender",
     *     type="string"
     *   ),
     *   @OA\Property(
     *     property="birth_date",
     *     type="date"
     *   ),
     * )
     * @OA\Response(
     *   response="UserResource",
     *   description="User object",
     *   @OA\JsonContent(
     *     @OA\Property(
     *       property="data",
     *       type="object",
     *       ref="#/components/schemas/User",
     *     )
     *   ),
     * )
     * @OA\Response(
     *   response="UserResourceColllection",
     *   description="A list of users",
     *   @OA\JsonContent(
     *     @OA\Property(
     *       property="data",
     *       type="array",
     *       @OA\Items(
     *         ref="#/components/schemas/User",
     *       ),
     *     )
     *   ),
     * )
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'                  => $this->id,
            'first_name'          => $this->first_name,
            'last_name'           => $this->last_name,
            'adv_notification'    => $this->adv_notification,
            'repair_notification' => $this->repair_notification,
            'email'               => $this->email,
            'phone'               => $this->phone,
            'role_name'           => $this->role_name,
            'address'             => $this->address,
            'gender'              => $this->gender,
            'birth_date'          => $this->birth_date,
            'service_orders'      => ServiceOrderResource::collection($this->whenLoaded('serviceOrders')),
        ];
    }
}
