<?php

namespace App\Models;

use App\Models\Traits\Filterable;
use App\Models\Traits\Sortable;
use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, HasRoles, Filterable, Sortable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'first_name',
        'last_name',
        'password',
        'phone',
        'default_address',
        'adv_notification',
        'repair_notification',
        'gender',
        'address',
        'birth_date'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'last_activity_at'  => 'datetime'
    ];

    protected $appends = [
        'role_name'
    ];

    public function getRoleNameAttribute()
    {
        return $this->getRoleNames()->first();
    }

    /**
     * @inheritDoc
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * @inheritDoc
     */
    public function getJWTCustomClaims(): array
    {
        return [
            'user' => [
                'id' => $this->id,
            ]
        ];
    }

    public function sendPasswordResetNotification($token): void
    {
        $createUrlCallback = function (CanResetPassword $notifiable, $token) {
            $baseFrontendUrl = config('app.frontend_url');
            $params = [
                'token' => $token,
                'email' => $notifiable->getEmailForPasswordReset()
            ];
            return $baseFrontendUrl . '/#/auth/password?' . http_build_query($params);
        };

        ResetPasswordNotification::$createUrlCallback = $createUrlCallback;

        parent::sendPasswordResetNotification($token);
    }

    public function cart()
    {
        return $this->hasOne(Cart::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'customer_id');
    }

    public function archived_user()
    {
        return $this->HasOne(ArchivedUser::class);
    }
}
