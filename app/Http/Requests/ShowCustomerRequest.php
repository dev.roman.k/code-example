<?php

namespace App\Http\Requests;

use App\Http\Requests\Traits\HasFilters as HasFiltersTrait;
use App\Http\Requests\Traits\HasIncludes as HasIncludesTrait;

class ShowCustomerRequest extends ShowRequest implements HasIncludes
{
    use HasIncludesTrait;

    /**
     * @return array
     */
    public function allowedIncludes(): array
    {
        return [
            'serviceOrders',
            'serviceOrders.serviceOrderItems',
            'serviceOrders.serviceOrderItems.service',
            'serviceOrders.serviceOrderItems.service.deviceModel'
        ];
    }
}
