<?php

namespace App\Http\Requests\Traits;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

trait HasIncludes
{
    /**
     * List of allowed include paths.
     * @return array
     */
    public function allowedIncludes(): array
    {
        // disable all includes by default
        return [];
    }

    /**
     * Validation rules for include array.
     * @return array
     */
    public function includeKeyRules(): array
    {
        return [
            'include'   => 'sometimes|required|array',
            'include.*' => 'required|string|min:0'
        ];
    }

    /**
     * List of request include paths.
     * @return array
     */
    public function getRequestIncludes(): array
    {
        return $this->get('include', []);
    }

    /**
     *
     * @return array
     */
    public function getIncludes(): array
    {
        return array_intersect($this->allowedIncludes(), array_values($this->getRequestIncludes()));
    }

    /**
     * @throws ValidationException
     */
    public function validateIncludes(): void
    {
        // add validation rules to validator
        /** @var Validator $validator */
        $validator = $this->getValidatorInstance();

        $rules = $this->includeKeyRules();
        $validator->addRules($rules);
        if ($validator->fails()) {
            $this->failedValidation($validator);
        }

        // validate keys
        $this->validateIncludeKeys();
    }

    /**
     * @throws ValidationException
     */
    private function validateIncludeKeys(): void
    {
        $requestInclude = $this->getRequestIncludes();
        $allowedIncludes = $this->allowedIncludes();
        foreach ($requestInclude as $include) {
            if (!in_array($include, $allowedIncludes, true)) {
                throw ValidationException::withMessages(['include' => "invalid include path ${include}"]);
            }
        }
    }
}
