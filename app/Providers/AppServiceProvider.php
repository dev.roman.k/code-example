<?php

namespace App\Providers;

use App\Classes\CurrentCart;
use App\Http\Requests\HasFilters;
use App\Http\Requests\HasIncludes;
use App\Http\Requests\HasSorts;
use App\Models\Order;
use App\Observers\OrderObserver;
use Faker\Factory;
use Faker\Generator;
use Faker\Provider\Youtube;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Generator::class, function ($app) {
            $faker = Factory::create();
            $faker->addProvider(new Youtube($faker));

            return $faker;
        });

        $this->app->singleton(CurrentCart::class, function ($app) {
            return new CurrentCart();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->afterResolving(HasIncludes::class, function (HasIncludes $resolved) {
            $resolved->validateIncludes();
        });
        $this->app->afterResolving(HasFilters::class, function (HasFilters $resolved) {
            $resolved->validateFilters();
        });
        $this->app->afterResolving(HasSorts::class, function (HasSorts $resolved) {
            $resolved->validateSorts();
        });

        Order::observe(OrderObserver::class);
    }
}
