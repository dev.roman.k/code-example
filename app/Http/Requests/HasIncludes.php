<?php

namespace App\Http\Requests;

interface HasIncludes
{
    public function validateIncludes();
}
