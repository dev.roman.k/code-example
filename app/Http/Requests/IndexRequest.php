<?php

namespace App\Http\Requests;

abstract class IndexRequest extends AuthorizeTrueRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [];
    }
}
