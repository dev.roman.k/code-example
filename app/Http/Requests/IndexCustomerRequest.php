<?php

namespace App\Http\Requests;

use App\Http\Requests\Traits\HasFilters as HasFiltersTrait;
use App\Http\Requests\Traits\HasIncludes as HasIncludesTrait;
use App\Http\Requests\Traits\HasSorts as HasSortsTrait;

class IndexCustomerRequest extends IndexRequest implements HasFilters, HasIncludes, HasSorts
{
    use HasFiltersTrait;
    use HasIncludesTrait;
    use HasSortsTrait;

    /**
     * @return array
     */
    public function allowedFilters(): array
    {
        return [
            'id'
        ];
    }

    /**
     * @return array
     */
    public function allowedIncludes(): array
    {
        return [
            'serviceOrders',
            'serviceOrders.serviceOrderItems',
            'serviceOrders.serviceOrderItems.service',
            'serviceOrders.serviceOrderItems.service.deviceModel'
        ];
    }

    /**
     * @return array
     */
    public function allowedSorts(): array
    {
        return [
            'first_name',
            'last_name',
            'phone'
        ];
    }
}
