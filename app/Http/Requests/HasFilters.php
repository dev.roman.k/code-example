<?php

namespace App\Http\Requests;

interface HasFilters
{
    public function validateFilters();
}
