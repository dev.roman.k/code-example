<?php

namespace App\Http\Requests\Traits;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

trait HasSorts
{
    /**
     * List of allowed attributes to sort.
     *
     * @return array
     */
    public function allowedSorts(): array
    {
        // restrict all attributes to sort by default
        return [];
    }

    public function sortKeyRules(): array
    {
        return [
            'sort' => 'sometimes|required|string'
        ];
    }

    public function getRequestSorts(): string
    {
        return $this->get('sort', '');
    }

    public function validateSorts(): void
    {
        // add validation rules to validator
        /** @var Validator $validator */
        $validator = $this->getValidatorInstance();

        $rules = $this->sortKeyRules();
        $validator->addRules($rules);
        if ($validator->fails()) {
            $this->failedValidation($validator);
        }

        $this->validateSortKeys();
    }

    public function getSorts(): array
    {
        $sorts = [];
        $requestSorts = $this->getRequestSorts();

        if (!empty($requestSorts)) {
            $sortSets = explode(',', $requestSorts);

            foreach ($sortSets as $sortSet) {
                $sorts[] = $this->parseSortSet($sortSet);
            }
        }

        return $sorts;
    }

    /**
     * @throws ValidationException
     */
    private function validateSortKeys(): void
    {
        $requestSorts = $this->getRequestSorts();
        if (!empty($requestSorts)) {
            $allowedSorts = $this->allowedSorts();

            $sortSets = explode(',', $requestSorts);

            $re = "/^(-?)(\w+)$/";
            foreach ($sortSets as $sortSet) {
                $parameter = $sortSet;
                if (strpos($sortSet, '-') === 0) {
                    $parameter = substr($parameter, 1);
                }

                $isAllowed = in_array($parameter, $allowedSorts, true);
                $isValid = preg_match($re, $sortSet);
                if (!$isAllowed || !$isValid) {
                    throw ValidationException::withMessages(['include' => "invalid sort parameter ${$parameter}"]);
                }
            }
        }
    }

    private function parseSortSet(string $sortSet): array
    {
        $match = [];
        preg_match("/^(?<direction>-?)(?<parameter>\w+)$/", $sortSet, $match);

        $parameter = $match['parameter'];

        $direction = 'asc';
        if (strpos($match['direction'], '-') === 0) {
            $direction = 'desc';
        }

        return [$parameter, $direction];
    }
}
